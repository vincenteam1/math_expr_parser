## Parser d'expression mathématiques

exemple d'utilisation:
```bash
make
./calc "2*3 + 2*(1+2)"
```
output:
```
...
0 (+) : 1, 10
1 (+) : 2, 3
2 (0) : -1, -1
3 (*) : 4, 5
4 (2) : -1, -1
5 (3) : -1, -1
10 (*) : 11, 12
11 (2) : -1, -1
12 (+) : 13, 17
13 (+) : 14, 15
14 (0) : -1, -1
15 (1) : -1, -1
17 (2) : -1, -1
Result : 12
```

le programme affiche une représentation de l'arbre syntaxique créé, chaque ligne contient: ```<identifiant du sommet> (<valeur associée au sommet>) : <identifiant du sous arbre de gauche> <identifiant du sous arbre de droite>``` quand il n'y a pas de sous arbre (dans le cas d'une feuille) -1 est affiché